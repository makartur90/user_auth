require('dotenv').config();
const cluster = require('node:cluster');
const http = require('node:http');
const numCPUs = require('node:os').availableParallelism();
const process = require('node:process');
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const CPUCount = numCPUs < process.env.CPU_COUNT ? numCPUs : process.env.CPU_COUNT;
const mysql = require('mysql')
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'oktemBer2',
    database: 'learn_node'
})

connection.connect()



connection.end()
if (cluster.isPrimary) {
    console.log(`Primary ${process.pid} is running`);

    // Форк рабочих.
    for (let i = 0; i < CPUCount; i++) {
        cluster.fork();
    }

    cluster.on('exit', (worker, code, signal) => {
        console.log(`рабочий ${worker.process.pid} умер`);
    });
} else {
    app.listen(port, () => {
        console.log(`Server is running on port ${port}`);
    });
    // http.createServer((req, res) => {
    //     res.writeHead(200);
    //     res.end('hello world\n');
    // }).listen(8000);

    console.log(`Worker ${process.pid} started`);
    console.log(process.env.FRONTEND_URL + '/success')
}

console.log(process.env.FRONTEND_URL + '/dashboard')
